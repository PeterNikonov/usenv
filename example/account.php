<?php

    session_start(); // user data stored in: $_SESSION[ \Usenv\Config::SESSION_KEY ];
    include './vendor/autoload.php';

    use \Usenv\Action;
    use \Usenv\Config;
    use \Usenv\Data;

    use \MyPractic\Stringf;
    use \MyPractic\Filef;

    use \Logenv\Data as Logdata;
    use \Licenv\Action as LicenceAction;

    try {

        $log = new Logdata;
        $log ->Init();

        $d = new \Usenv\Data;
        $d -> init();

    } catch (Exception $ex) {
        print $ex->getTraceAsString();
    }

    $action = new Action;
    
    function Captcha() {
        if($_SESSION['captcha']!== $_POST['captcha']) { throw new Exception ('bad code'); }
    }

    // регистрация
    if(isset($_GET['registration'])) {
        if(!empty($_POST)) {
            try {
                Captcha();
                $user_id = $action -> Registration($_POST);
                $text = Stringf::k2v(Filef::Read(Config::GetDir().'/html/mail_hello.html'), [
                        'name' => $_POST['name'],
                        'email' => $_POST['email'],
                        'password' => $_POST['password'],
                    ]);
                mail($_POST['email'], 'Регистрация учетной записи', $text);
                $log ->Set($user_id, 'Регистрация');

            } catch (Exception $ex) { print $ex->getMessage(); }
        } else {
            // форма регистрации
            print Filef::Read(Config::GetDir().'/html/reg.html');
        }
    }

    // войти
    if(isset($_GET['login'])) {
        if(!empty($_POST)) {
            try {
                $user_id = $action->Login($_POST['email'], $_POST['password'], (isset($_POST['userem'])));
                $log ->Set($user_id, 'Вход выполнен');
                print 'вход выполнен';
            } catch (Exception $ex) { print $ex->getMessage(); }
        } else {
               // войти через куку
               $user_id = $action->HashLogin();
            if($user_id) {
                $log ->Set($user_id, 'Вход выполнен');
                print 'вход выполнен';
            } else {
              print Filef::Read(Config::GetDir().'/html/login.html');
            }
        }
    }

    // сброс пароля
    if(isset($_GET['reminder'])) {
        if(!empty($_POST)) {
            if(isset($_GET['hash'])) {
                // выполнить обновление пароля ШАГ 4
                    // записать в лог
                try {
                    $user_id = $action->PasswordResetPut($_GET['hash'], $_POST['password']);
                    $log ->Set($user_id, 'Изменен пароль');
                } catch (Exception $ex) { print $ex->getMessage(); print $ex->getTraceAsString(); }
            } else {
                // отправить письмо с хешем на ввод пароля ШАГ 2
                    // записать в лог 
                try {
                    $hash = $action -> PasswordResetInit($_POST['email']);
                    $text = Stringf::k2v(Filef::Read(Config::GetDir().'/html/mail_reminder.html'), [
                        'domain' => Config::DOMAIN,
                        'hash' => $hash,
                    ]);
                    mail($_POST['email'], 'Восстановление пароля', $text);
                } catch (Exception $ex) { print $ex->getMessage(); }
            }
        } else {
            if(isset($_GET['hash'])) {
                // форма ввода нового пароля ШАГ 3
                // проверить хеш
                try {
                    $action->PasswordResetCheck($_GET['hash']);
                    print Stringf::k2v(Filef::Read(Config::GetDir().'/html/password.html'), ['hash'=>$_GET['hash']]);
                } catch (Exception $ex) { print $ex->getMessage(); print $ex->getTraceAsString(); }
            } else {
                print Filef::Read(Config::GetDir().'/html/reminder.html');
            }
        }
    }

    // выйти
    if(isset($_GET['logout'])) { $action->Logout(); print 'вышли из '; }
