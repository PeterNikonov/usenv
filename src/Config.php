<?php

namespace Usenv;

/**
 * Description of Usenv
 *
 * @author Dell
 */
class Config {
    /**
     * Ключ куки запомни меня
     */
    const COOKIE_KEY = 'usenv';
    /**
     * Ключ сессии с информацией о пользователе
     */
    const SESSION_KEY = 'user';
    /**
     * Название таблицы в бд с информацией о пользователе
     */
    const USER_TABLE = 'env_user';
    /**
     * Название таблицы в бд с авторизационными ключами
     */
    const HASH_TABLE = 'env_hash';
    /**
     * Домен для ссылки, восстановление пароля
     */
    const DOMAIN = 'envuser';
    /**
     * абсолютный путь к файлам пакета
     */
    public static function GetDir() { return __DIR__; }

}
