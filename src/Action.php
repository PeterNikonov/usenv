<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Usenv;

/**
 * Description of UserOperationAction
 *
 * @author Dell
 */
use \Usenv\Config;
use \Usenv\Data;

class Action {

    /**
     * Войти в программу по токену
     */
    public function HashLogin() {

        if(isset($_COOKIE[Config::COOKIE_KEY])) {

                 $model = new Data;
           $ch = $model->checkHash($_COOKIE[Config::COOKIE_KEY]);
        if($ch) {
              $user = $model->checkUser(FALSE, FALSE, $ch->user);
           if($user) {
              $_SESSION[Config::SESSION_KEY] = $user;
              return $user['id'];
                }    
            }
        } else { return FALSE; }
    }

    /**
     * Войти в программу по логину и паролю
     */
    public function Login($email, $password, $userem = false) {

           $model = new Data;
           $user = $model->checkUser($email, $password);
        if($user) {
            $_SESSION[Config::SESSION_KEY] = $user;
            if($userem) { $this->Remember(); }
            return $user['id'];
        } else { throw new \Exception('bad passw'); }
    }
    /**
     * Запомнить меня
     */
    public function Remember() {

           $user = $_SESSION[Config::SESSION_KEY];
        if($user) {

            $model = new Data;
            $hash = $model->hashGen();

            $model ->setHash($user['id'], $hash);
            setcookie(Config::COOKIE_KEY, $hash, (time()+60*60*24*90));
        }
    }
    /**
     * Выйти из программы
     */
    public function Logout() {
        
        $model = new Data;
        $model->DeleteHash($_COOKIE[Config::COOKIE_KEY]);
        unset($_SESSION[Config::SESSION_KEY]);
        setcookie(Config::COOKIE_KEY, '', (time()-1000));
    }
    /**
     * Регистрация нового пользователя
     */
    public function Registration($data) {

                     $model = new Data;
            $check = $model->checkUser($data['email']);
        if(!$check) {
            return $model->createNewUser($data);
        } else { throw new \Exception('email was register early'); }
    }
    /**
     * Сменить пароль - шаг 1 - инициализировать хеш
     */
    public function PasswordResetInit($email) {

        $model = new Data;

           $user = $model->checkUser($email);
        if($user) {

            $hash = $model->hashGen();
            $model ->setHash($user['id'], $hash);
            return $hash;

        } else { throw new \Exception('user not found'); }
    }
    /**
     * Сменить пароль - шаг 2 проверить хеш
     */
    public function PasswordResetCheck($hash) {

                 $model = new Data;
           $ch = $model->checkHash($hash);
        if($ch) { return TRUE; } 
        else { throw new \Exception('hash not found'); }

    }

    /**
     * Сменить пароль - шаг 3 проверить хеш
     */
    public function PasswordResetPut($hash, $password) {
                 $model = new Data;
           $ch = $model->checkHash($hash);
        if($ch) {
            $model->setUserPassword($ch['user'], $password);
            $model->DeleteHash($hash);
            return $ch['user'];
        } else { throw new \Exception('hash not found'); }
    }
}
