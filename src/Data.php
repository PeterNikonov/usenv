<?php

namespace Usenv;

use \My\MyPdo;
use \MyPractic\MyQuery;
use \Usenv\Config;
use \PDO;

class Data {

    public function hashGen() {
        return md5(serialize(@range((rand(0, 100)),rand(100,1000),rand(0, 100))));
    }

    /**
    * Создать таблицы для хранения данных о пользователях
    */
   public function init() {

    // учетные записи
    $q = "CREATE TABLE IF NOT EXISTS `".  Config::USER_TABLE ."` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `name` varchar(256) NOT NULL,
    `email` varchar(256) NOT NULL,
    `password` varchar(256) NOT NULL,

    PRIMARY KEY (`id`),
    KEY `search` (`email`, `password`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

    MyPdo::get()->query($q);

    $q = "CREATE TABLE IF NOT EXISTS `". Config::HASH_TABLE ."` (
`id` int(14) NOT NULL AUTO_INCREMENT,
`user` INT(14),
`hash` varchar(256) NOT NULL,
`insdate` date NOT NULL,

KEY (`hash`),

  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='';";

    MyPdo::get()->query($q);

    }

    /**
     * Проверить пользователя
     */
    public function checkUser($email = FALSE, $password = FALSE, $id = FALSE) {

        $email_clause    = ($email)    ? "email = ". MyPdo::get()->quote($email) : '';
        $password_clause = ($password) ? "AND password = '". md5($password) ."'" : '' ;
        $id_clause       = ($id)       ? "id = '". (int) $id ."'" : '' ;

        $stmt = MyPdo::get()->query("SELECT * FROM ". Config::USER_TABLE ." WHERE "
        . $id_clause
        . $email_clause
        . $password_clause);

        if($stmt) {
            $user = $stmt -> fetch(PDO::FETCH_ASSOC);
        }
        return $user;
    }

    /**
     * Зарегистрировать пользователя
     */
    public function createNewUser($data) {

        $data['password'] = md5($data['password']);
        MyPdo::get()->query((new MyQuery(Config::USER_TABLE, $data))-> Insert());

               $userId = MyPdo::get()->lastInsertId();
        return $userId;
    }

    /**
     * Установить новый пароль
     */
    public function setUserPassword($id, $password) {
        $data['password'] = md5($password);
        MyPdo::get()->query((new MyQuery(Config::USER_TABLE, $data))->Update($id));
    }

    /**
     * Зарегистрировать хеш 
     */
    public function setHash($id, $hash) {
        $data['user'] = $id;
        $data['hash'] = $hash;
        MyPdo::get()->query((new MyQuery(Config::HASH_TABLE, $data))->Insert());
    }

    /**
     * Проверить хеш
     */
    public function checkHash($hash) {
           $stmt = MyPdo::get()->query("SELECT * FROM ".Config::HASH_TABLE." WHERE hash = ".MyPdo::get()->quote($hash)."");
        if($stmt) {
            $row = $stmt -> fetch(PDO::FETCH_ASSOC);
        }
        return $row;
    }
    /**
     * Удалить хеш из бд
     */
    public function DeleteHash($hash) {
        MyPdo::get()->query("DELETE FROM ".Config::HASH_TABLE." WHERE hash = ".MyPdo::get()->quote($hash)."");
    }
}
